# Second JavaScript CRUD ReST API 

## Change bettween the other backend

* We will create 2 JavaScript API ReST as backend, to integrate a fullstack application. 
  
* On the other JavaScript NodeJS API ReST, we work on **Express**, **MongoDB** & **Mongoose ODM**. 
* To compare with other tools, understand & learn on differents , datas persistence SGBD & objects   I will using PostgreSql instind MongoDB & Sequelize ORM, instead Mongoose ODM.

* we will create 5 endpoints for basic CRUD operations:

    - **Create**
    - **Read all**
    - **Read one**
    - **Update**
    - **Delete**


* **Node.js stack**, this application will using:

    - **Express** as a framework
    - **Sequelize** as an **ORM**
    - **Docker** & **Docker compose** to dockerize this Node.js application, (create a ``docker compose`` file to run both the services)
    - **Postgres** to persiste datas (we'll test it with **Tableplus**)

    ...

    - We will test the API endpoints with **Postman**

## PostgreSQL install. & run (on Linux mint distrib.)

```sh
# Update and upgrade linux mint:
sudo apt update && sudo apt upgrade

# Install PostgreSQL
sudo apt install postgresql

# Start PostgreSQL
sudo service postgresql start

# Status PostgreSQL
sudo service postgresql status
# return following informations, on my equipement :
    # ● postgresql.service - PostgreSQL RDBMS
    # Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
    # Active: active (exited) since Wed 2023-11-08 21:26:16 GMT; 19min ago
    # Process: 21045 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
    # Main PID: 21045 (code=exited, status=0/SUCCESS)
    # CPU: 2ms
    ...

# Stop PostgreSQL
sudo service postgresql stop
```